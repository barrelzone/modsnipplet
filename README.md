# ModSnipplet

Simply build the MyMod and the Target projects. After that, copy MyMod.dll from the MyMod/bin/Debug/MyMod.dll to the directory: Target/bin/Debug/MyMod/ (you'll have to make a new directory.)

After that, it should work.

See also:

MarshalByRefObject: Required to reference objects across AppDomains.
https://docs.microsoft.com/en-us/dotnet/api/system.marshalbyrefobject?view=netframework-4.7.2

Serializable: Required to use objects in dynamically loaded assemblies.
https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/concepts/serialization/