﻿using System;

namespace ModAPI
{
    [Serializable]
    public class Foo : MarshalByRefObject
    {
        public event EventHandler<bool> InitEvent;

        public void OnInit()
        {
            Console.WriteLine("Starting Foo.Init()");
            InitEvent.Invoke(this, true);
        }

        public void Subscribe(System.EventHandler<bool> handler)
        {
            InitEvent += handler;
        }
    }
}
