﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModAPI
{
    public class Mod : MarshalByRefObject
    {
        protected ModAPI.Foo _foo;
        public Mod(ModAPI.Foo foo)
        {
            _foo = foo;
        }
        public virtual void Load() { }
        public virtual void Unload() { }
    }
}
