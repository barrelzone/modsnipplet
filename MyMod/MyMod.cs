﻿using System;
using System.Reflection;

namespace MyMod
{
    [Serializable]
    public class MyMod : ModAPI.Mod
    {
        public MyMod(ModAPI.Foo foo) : base(foo) { }

        public override void Load()
        {
            _foo.InitEvent += Foo_OnInit;
        }

        private void Foo_OnInit(object sender, bool e)
        {
            Console.WriteLine("HELLO FROM MY MOD!");
        }

        public override void Unload()
        {
        }
    }
}
