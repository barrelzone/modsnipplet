﻿using System;
using System.Reflection;

namespace Target
{
    class Program
    {
        public static int Main(string[] args)
        {
            ModAPI.Foo MyFoo = new ModAPI.Foo();

            MyFoo.InitEvent += EventMgr_OnInit;

            // Load the AppDomain
            AppDomain ad = AppDomain.CreateDomain("ModDomain");
            ModAPI.Mod mod = (ModAPI.Mod)Loader.Call("MyMod", "MyMod.MyMod", ad, MyFoo);
            mod.Load();

            // Do Init.
            MyFoo.OnInit();

            mod.Unload();
            AppDomain.Unload(ad);

            Console.ReadKey();
            return 0;
        }

        private static void EventMgr_OnInit(object sender, bool e)
        {
            Console.WriteLine("This is a test.");
        }
    }
}
