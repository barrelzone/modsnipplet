﻿using System;
using System.Reflection;

namespace Target
{
    public class Loader : MarshalByRefObject
    {
        object _Call(string dll, string typename, ModAPI.Foo foo)
        {
            Assembly a = Assembly.Load(dll);
            object[] args = new object[1];
            args[0] = foo;
            return a.CreateInstance(typename, false, BindingFlags.CreateInstance, null, args, null, null);
        }

        public static object Call(string dll, string typename, AppDomain dom, ModAPI.Foo foo)
        {
            Loader ld = (Loader)dom.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, typeof(Loader).FullName);
            object result = ld._Call(dll, typename, foo);
            return result;
        }
    }
}
